import 'package:cached_network_image/cached_network_image.dart';
import 'package:church_app/src/church_app_colors.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/constants.dart';
import 'package:flutter/material.dart';

class ListTileCard extends StatelessWidget {
  final String name;
  final String date;
  final String imageUrl;
  final String assetUrl;
  final double radius;
  final double carouselSize;
  final bool useAssetImage;

  const ListTileCard({
    Key key,
    this.name,
    this.date,
    this.radius,
    this.imageUrl,
    this.carouselSize,
    this.assetUrl,
    this.useAssetImage = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(.0),
      child: Card(
        elevation: 8,
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Material(
                elevation: 7,
                borderRadius: BorderRadius.circular(50),
                child: SizedBox(
                    height: 70,
                    width: 70,
                    child: Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: !useAssetImage
                            ? ClipRRect(
                                borderRadius: BorderRadius.circular(50),
                                child: CachedNetworkImage(
                                  imageUrl: imageUrl,
                                  fit: BoxFit.fill,
                                ),
                              )
                            : Image.asset(
                                assetUrl,
                                scale: 20,
                              ))),
              ),
            ),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    width: 8,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
                    child: Text(
                      this.name,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  this.date != null
                      ? Padding(
                          padding:
                              const EdgeInsets.only(left: 8.0, bottom: 8.0),
                          child: Text(
                            this.date,
                            style: TextStyle(
                                color: ChurchAppColors.navyBlue, fontSize: 10),
                          ),
                        )
                      : Container()
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
