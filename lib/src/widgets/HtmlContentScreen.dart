import 'package:church_app/src/screens/old_dev_non_bloc/utils/constants.dart';
import 'package:church_app/src/widgets/html_view.dart';
import 'package:flutter/material.dart';
import 'package:church_app/src/church_app_colors.dart';
import 'package:cached_network_image/cached_network_image.dart';

class HtmlContentScreenBasic extends StatelessWidget {
  final String name;
  final String content;
  final String date;
  final String image;
  final String title;
  const HtmlContentScreenBasic(
      {Key key, this.name, this.date, this.content, this.image, this.title})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: CachedNetworkImage(
                imageUrl: image,
                fit: BoxFit.fill,
              ),
            ),
            Expanded(
              child: HtmlView(
                html: this.content ?? 'No content',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
