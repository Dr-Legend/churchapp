import 'package:church_app/src/screens/settings/settings_bloc.dart';
import 'package:church_app/src/screens/settings/settings_screen.dart';
import 'package:church_app/src/screens/settings/settings_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MainDrawer extends StatefulWidget {
  @override
  _MainDrawerState createState() => _MainDrawerState();
}

class _MainDrawerState extends State<MainDrawer> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: BlocBuilder<SettingsBloc, SettingsState>(
        builder: (BuildContext context, SettingsState state) =>
            SettingsScreen(settingsState: state),
      ),
    );
  }
}
