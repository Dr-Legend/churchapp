import 'package:cached_network_image/cached_network_image.dart';
import 'package:church_app/src/church_app_colors.dart';
import 'package:church_app/src/models/blog_data.dart';
import 'package:church_app/src/screens/blog/blog_bloc.dart';
import 'package:church_app/src/screens/home/home_screen.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/constants.dart';
import 'package:church_app/src/widgets/HtmlContentScreen.dart';
import 'package:church_app/src/widgets/html_view.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:church_app/src/screens/blog/blog_state.dart';

class Blog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final double _radius = 25.0;
    final double _screenHeight = MediaQuery.of(context).size.height;
    final double _carouselSize = _screenHeight / 2.1;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).canvasColor,
          title: Text("Blog"),
        ),
        body: BlocBuilder<BlogBloc, BlogState>(
          builder: (BuildContext context, BlogState state) {
            if (state is LoadingBlogState) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is ErrorBlogState) {
              return Center(
                child: Text("Unable to fetch blogs please try again later.."),
              );
            }
            if (state is LoadedBlogState) {
              return Container(
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: ListView.builder(
                        shrinkWrap: false,
                        itemCount: state.blogData.length ?? 0,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => HtmlContentScreenBasic(
                                        date: state.blogData[index].blogDate,
                                        title: 'Blog',
                                        name: state.blogData[index].name,
                                        image: Constants.BLOG_THUMBNAIL +
                                            state.blogData[index].image,
                                        content: state.blogData[index].content,
                                      )));
                            },
                            child: BlogCard(
                              blogData: state.blogData[index],
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              );
            }
            return Container(
              child: Center(
                child: Text("you shoudn't see this"),
              ),
            );
          },
        ),
      ),
    );
  }
}

class BlogCard extends StatelessWidget {
  final BlogData blogData;
  final double radius;
  final double carouselSize;

  const BlogCard({Key key, this.blogData, this.radius, this.carouselSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(.0),
      child: Card(
        elevation: 8,
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Material(
                elevation: 7,
                borderRadius: BorderRadius.circular(50),
                child: SizedBox(
                    height: 70,
                    width: 70,
                    child: Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(50),
                          child: CachedNetworkImage(
                            imageUrl:
                                "${Constants.BLOG_THUMBNAIL}${this.blogData.image}",
                            fit: BoxFit.fill,
                          ),
                        ))),
              ),
            ),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                    width: 8,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
                    child: Text(
                      this.blogData.name,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
                    child: Text(
                      this.blogData.blogDate,
                      style: TextStyle(
                          color: ChurchAppColors.navyBlue, fontSize: 10),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

//class BlogContentScreen extends StatelessWidget {
//  final BlogData blogData;
//
//  const BlogContentScreen({Key key, this.blogData}) : super(key: key);
//  @override
//  Widget build(BuildContext context) {
//    final double _radius = 25.0;
//    final double _screenHeight = MediaQuery.of(context).size.height;
//    final double _carouselSize = _screenHeight / 2.1;
//    return SafeArea(
//      child: Scaffold(
//        body: NestedScrollView(
//          body: Container(
//            padding: EdgeInsets.all(8),
//            child: Text(
//              blogData.content,
//              style: TextStyle(
//                color: Color(0xFF5A6571),
//                fontFamily: "Quantify",
//              ),
//            ),
//          ),
//          headerSliverBuilder: (context, innerBoxScrolled) {
//            return <Widget>[
//              SliverAppBar(
//                floating: true,
//                pinned: true,
//                elevation: 0.0,
//                backgroundColor: Colors.transparent,
//                expandedHeight: _screenHeight / 2 - 50,
//                flexibleSpace: Hero(
//                    tag: "Carouselcard${blogData.name}",
//                    child: Image.network(
//                      "${Constants.BLOG_THUMBNAIL}${blogData.image}",
//                      fit: BoxFit.cover,
//                    )),
//              )
//            ];
//          },
//        ),
//      ),
//    );
//  }
//}
