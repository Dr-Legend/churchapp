import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:church_app/src/api/api.dart';
import 'package:church_app/src/models/blog_data.dart';
import 'package:church_app/src/screens/blog/blog_event.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/constants.dart';
import 'package:meta/meta.dart';
import 'package:church_app/src/screens/blog/blog_state.dart';

class BlogBloc extends Bloc<BlogEvent, BlogState> {
  final ApiMethods api = ApiMethods();
  @override
  BlogState get initialState => InitialBlogState();

  @override
  Stream<BlogState> mapEventToState(BlogEvent event) async* {
    if (event is LoadBlogEvent) {
      yield LoadingBlogState();
      try {
        String response = await api.requestInGet(Constants.BLOG_API);
        List<BlogData> blogs = blogDataFromJson(response);
        yield LoadedBlogState(blogs);
      } catch (e) {
        yield ErrorBlogState(e);
      }
    }
  }
}
