import 'package:church_app/src/models/blog_data.dart';
import 'package:meta/meta.dart';

@immutable
abstract class BlogState {}

class InitialBlogState extends BlogState {}

class LoadingBlogState extends BlogState {}

class LoadedBlogState extends BlogState {
  final List<BlogData> blogData;

  LoadedBlogState(this.blogData);
}

class ErrorBlogState extends BlogState {
  final String error;

  ErrorBlogState(this.error);
}
