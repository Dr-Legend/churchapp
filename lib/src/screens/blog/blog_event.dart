import 'package:meta/meta.dart';

@immutable
abstract class BlogEvent {}

@immutable
class LoadBlogEvent extends BlogEvent {}
