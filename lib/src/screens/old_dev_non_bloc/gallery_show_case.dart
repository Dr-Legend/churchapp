import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:church_app/src/models/gallery_data.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/gallery_images_page.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/presenters/common_presenter.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/constants.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/enum_pages.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/utility.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/views/common_view.dart';
import 'package:flutter/material.dart';

class GalleryShowCase extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _GalleryShowState();
  }
}

class _GalleryShowState extends State<GalleryShowCase> implements View {
  Presenter _presenter;
  List<GalleryData> _dataList;

  _GalleryShowState() {
    _presenter = new Presenter(this);
    _dataList = new List<GalleryData>();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GridView.count(
      crossAxisCount: 3,
      children: List.generate(_dataList.length, (index) {
        return _gridItems(index);
      }),
    );
  }

  ///Call the API here
  @override
  void initState() {
    super.initState();
    setState(() {
      Utility.check().then((internet) {
        if (internet != null && internet) {
          Future.delayed(Duration.zero,
              () => Utility.showProgressDialog(Constants.PLEASE_WAIT, context));
          _presenter.makeGetCall(
              Constants.GALLERY_ALBUM_API, PageName.LIVE_VIDEO);
        } else {
          Utility.showToast(Constants.NO_INTERNET_FOUND, context);
        }
      });
    });
  }

  @override
  onFailed(Exception onError) {
    Utility.dismissProgress(context);
    Utility.showToast(Constants.SOMETHING_WENT, context);
  }

  @override
  onSuccess(String data, PageName pageName) {
    Utility.dismissProgress(context);
    setState(() {
      try {
        _dataList = galleryDataFromJson(data);
      } on TypeError {
        Utility.showToast(Constants.NO_DATA, context);
      }
    });
  }

  Widget _gridItems(int index) {
    var size = MediaQuery.of(context).size;
    final double itemHeight = (size.height - kToolbarHeight - 24) / 3;
    final double itemWidth = size.width / 3;
    return InkWell(
      child: Padding(
        padding: const EdgeInsets.only(left: 4.0, right: 4.0),
        child: Center(
          child: Card(
            elevation: 5,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Expanded(
                    child: Stack(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 4.0),
                      child: Container(
                        height: itemHeight,
                        width: itemWidth,
                        child: CachedNetworkImage(
                          fit: BoxFit.fill,
                          imageUrl: Constants.GALLERY_ALBUM_THUMBNAIL +
                              _dataList[index].image,
                          placeholder: (context, url) => Image.asset(
                            'images/ic_placeholder.png',
                            fit: BoxFit.fill,
                          ),
                          errorWidget: (context, url, error) => Image.asset(
                              'images/ic_placeholder.png',
                              fit: BoxFit.fill,
                              width: itemWidth),
                        ),
                      ),
                    ),
                  ],
                )),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Center(
                      child: Text(
                    _dataList[index].name,
                    maxLines: 1,
                  )),
                ),
              ],
            ),
          ),
        ),
      ),
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    GalleryImagesPage(data: _dataList[index])));
      },
    );
  }
}
