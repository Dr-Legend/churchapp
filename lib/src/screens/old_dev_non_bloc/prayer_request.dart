import 'package:church_app/src/models/custom_data.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/components/button_widget.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/components/text_field_widget.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/presenters/common_presenter.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/constants.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/enum_pages.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/utility.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/views/common_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_country_picker/flutter_country_picker.dart';

class PrayerRequestScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _PrayerState();
  }
}

class _PrayerState extends State<PrayerRequestScreen> implements View {
  var _validateName = true;
  var _validateEmail = true;
  var _validateContact = true;
  var _validateState = true;
  var _validateCity = true;
  var _validateMessage = true;

  final _controllerContact = TextEditingController();
  final _controllerFullName = TextEditingController();
  final _controllerState = TextEditingController();
  final _controllerCity = TextEditingController();
  final _controllerMessage = TextEditingController();
  final _controllerEmail = TextEditingController();

  var errorMessage = Constants.EMPTY_TEXT_ERROR;

  Presenter _presenter;
  List<String> _listCountryName;
  List<String> _listPrayerFor;
  var _pos = 0;
  var _posPrayer = 0;
  Country _selectedCountryName;
  String _countryName = 'UK';

  @override
  void initState() {
    super.initState();
    _presenter = new Presenter(this);
    _listPrayerFor = new List<String>();
    _addPrayerFor();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              EditTextWidget(
                keyboardType: TextInputType.text,
                isObscureText: false,
                labelText: 'Name',
                errorText: errorMessage,
                controller: _controllerFullName,
                errorValidation: _validateName,
                maxLines: 1,
              ),
              EditTextWidget(
                keyboardType: TextInputType.emailAddress,
                isObscureText: false,
                labelText: 'Email*',
                errorText: errorMessage,
                controller: _controllerEmail,
                errorValidation: _validateEmail,
                maxLines: 1,
              ),
              EditTextWidget(
                keyboardType: TextInputType.text,
                isObscureText: false,
                labelText: 'Contact Number*',
                errorText: errorMessage,
                controller: _controllerContact,
                errorValidation: _validateContact,
                maxLines: 1,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: new Container(
                  child: CountryPicker(
                    showDialingCode: false,
                    showFlag: true,
                    showName: true,
                    onChanged: (Country country) {
                      setState(() {
                        _selectedCountryName = country;
                        _countryName = country.name;
                      });
                    },
                    selectedCountry: _selectedCountryName,
                  ),
                ),
              ),
              EditTextWidget(
                keyboardType: TextInputType.text,
                isObscureText: false,
                labelText: 'State*',
                errorText: errorMessage,
                controller: _controllerState,
                errorValidation: _validateState,
                maxLines: 1,
              ),
              EditTextWidget(
                keyboardType: TextInputType.text,
                isObscureText: false,
                labelText: 'City*',
                errorText: errorMessage,
                controller: _controllerCity,
                errorValidation: _validateCity,
                maxLines: 1,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                child: DropdownButton<String>(
                  value: _listPrayerFor == null
                      ? null
                      : _listPrayerFor[_posPrayer],
                  items: _listPrayerFor.map((String value) {
                    return new DropdownMenuItem<String>(
                      value: value,
                      child: new Text(
                        value,
                      ),
                    );
                  }).toList(),
                  onChanged: (value) {
                    setState(() {
                      _posPrayer = _listPrayerFor.indexOf(value);
                    });
                  },
                ),
              ),
              EditTextWidget(
                keyboardType: TextInputType.text,
                isObscureText: false,
                labelText: 'Message*',
                errorText: errorMessage,
                controller: _controllerMessage,
                errorValidation: _validateMessage,
                maxLines: 6,
              ),
              ButtonWidget(
                padding: EdgeInsets.all(20.0),
                onPressed: () {
                  setState(() {
                    _submitRequest();
                  });
                },
                text: Text(
                  'SUBMIT',
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  bool _validViews() {
    var isValid = true;
    _validateName = true;
    _validateEmail = true;
    _validateContact = true;
    _validateState = true;
    _validateCity = true;
    _validateMessage = true;

    if (_controllerFullName.text.isEmpty) {
      _validateName = false;
      isValid = false;
    } else if (_controllerEmail.text.isEmpty) {
      _validateEmail = false;
      isValid = false;
    } else if (_controllerContact.text.isEmpty) {
      _validateContact = false;
      isValid = false;
    } else if (_controllerState.text.isEmpty) {
      _validateState = false;
      isValid = false;
    } else if (_controllerCity.text.isEmpty) {
      _validateCity = false;
      isValid = false;
    } else if (_controllerMessage.text.isEmpty) {
      _validateMessage = false;
      isValid = false;
    }
    return isValid;
  }

  void _submitRequest() {
    if (_validViews()) {
      if (_listPrayerFor[_posPrayer] != "Prayer For*") {
        var url = "firstName=" +
            _controllerFullName.text.trim() +
            "&state=" +
            _controllerState.text.trim() +
            "&city=" +
            _controllerCity.text.trim() +
            "&contact_no=" +
            _controllerContact.text.trim() +
            "&email=" +
            _controllerEmail.text.trim() +
            "&time=" +
            _listPrayerFor[_posPrayer] +
            "&message=" +
            _controllerMessage.text.trim() +
            "&country=" +
            _countryName;
        Utility.check().then((internet) {
          if (internet != null && internet) {
            Utility.showProgressDialog(Constants.PLEASE_WAIT, context);
            _presenter.makeGetCall(Constants.PRAYER_API + url, PageName.MUSIC);
          } else {
            Utility.showToast(Constants.NO_INTERNET_FOUND, context);
          }
        });
      } else {
        Utility.showAlertDialog(context,
            'Please select the reson for prayer request', "Alert!", 'Okay');
      }
    }
  }

  @override
  onFailed(Exception onError) {
    Utility.dismissProgress(context);
    Utility.showToast(Constants.SOMETHING_WENT, context);
  }

  @override
  onSuccess(String data, PageName _pageName) {
    print(data);
    Utility.dismissProgress(context);
    if (customDataFromJson(data).status == 'success') {
      Utility.showAlertDialog(
          context, Constants.THANK_YOU_TEXT, 'Thank You!', 'Okay');
      _controllerFullName.text = "";
      _controllerEmail.text = "";
      _controllerState.text = "";
      _controllerCity.text = "";
      _controllerContact.text = "";
      _controllerMessage.text = "";
    } else {
      Utility.showToast(Constants.SOMETHING_WENT, context);
    }
  }

  void _addPrayerFor() {
    _listPrayerFor.add('Prayer For*');
    _listPrayerFor.add('A Neighbour/Co-Worker');
    _listPrayerFor.add('Abuse');
    _listPrayerFor.add('Alchol');
    _listPrayerFor.add('Cancer');
    _listPrayerFor.add('Child Custody');
    _listPrayerFor.add('Church');
    _listPrayerFor.add('Cold & Flu');
    _listPrayerFor.add('Deliverance from Addictions');
    _listPrayerFor.add('Depression');
    _listPrayerFor.add('Diabetes');
    _listPrayerFor.add('Drugs');
    _listPrayerFor.add('Emotional Distress');
    _listPrayerFor.add('Family Member');
    _listPrayerFor.add('Family Situation');
    _listPrayerFor.add('For a Brother/Sister in Christ');
    _listPrayerFor.add('For a Friend');
    _listPrayerFor.add('For a Loved One');
    _listPrayerFor.add('For Finance');
    _listPrayerFor.add('For My Business');
    _listPrayerFor.add('For My Family');
    _listPrayerFor.add('For My Home Church');
    _listPrayerFor.add('For My Job');
    _listPrayerFor.add('For My Ministry');
    _listPrayerFor.add('For Myself');
    _listPrayerFor.add('For Provision');
    _listPrayerFor.add('For Current World Situation');
    _listPrayerFor.add('For the Government');
    _listPrayerFor.add('Heart Problem');
    _listPrayerFor.add('Internal Organs');
    _listPrayerFor.add('Legal Situation');
    _listPrayerFor.add('Life Threates');
    _listPrayerFor.add('Lung Disease');
    _listPrayerFor.add('Marriage Restoration');
    _listPrayerFor.add('Mental Illness');
    _listPrayerFor.add('Military Service');
    _listPrayerFor.add('Others');
    _listPrayerFor.add('Physical Ailment');
    _listPrayerFor.add('Protection');
    _listPrayerFor.add('Rebellious Children');
    _listPrayerFor.add('Salvation');
    _listPrayerFor.add('Sexual Perversion');
    _listPrayerFor.add('Stroke');
    _listPrayerFor.add('Tabacco');
  }
}
