import 'package:church_app/src/models/prayer_data.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/components/button_widget.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/components/text_field_widget.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/presenters/common_presenter.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/constants.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/enum_pages.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/utility.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/views/common_view.dart';
import 'package:flutter/material.dart';

class PrayerPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _PrayerPageState();
  }
}

///State for VideoShowCase Class

class _PrayerPageState extends State<PrayerPage> implements View {
  Presenter _presenter;
  PrayerData _prayerData;
  final _controllerPersonPhone = TextEditingController();
  final _controllerFullName = TextEditingController();
  final _controllerMessage = TextEditingController();
  final _controllerEmail = TextEditingController();
  var errorMessage = Constants.EMPTY_TEXT_ERROR;

  var _validateName = true;
  var _validateEmail = true;
  var _validatePhone = true;
  var _validateMessage = true;

  _PrayerPageState() {
    _presenter = new Presenter(this);
    _prayerData = new PrayerData();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: ListView(children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: SizedBox(
            height: 140,
            width: 140,
            child: Image.asset(
              'images/statement_of_faith.png',
            ),
          ),
        ),
        EditTextWidget(
          errorValidation: _validateName,
          controller: _controllerFullName,
          errorText: errorMessage,
          labelText: 'Full Name*',
          keyboardType: TextInputType.text,
          isObscureText: false,
        ),
        EditTextWidget(
            errorValidation: _validateEmail,
            controller: _controllerEmail,
            errorText: errorMessage,
            labelText: 'Email*',
            keyboardType: TextInputType.emailAddress,
            isObscureText: false),
        EditTextWidget(
            errorValidation: _validatePhone,
            controller: _controllerPersonPhone,
            errorText: errorMessage,
            labelText: 'Contact No*',
            keyboardType: TextInputType.number,
            isObscureText: false),
        EditTextWidget(
            errorValidation: _validateMessage,
            controller: _controllerMessage,
            errorText: errorMessage,
            labelText: 'Message*',
            keyboardType: TextInputType.text,
            isObscureText: false),
        ButtonWidget(
            onPressed: () => {
                  setState(() {
                    _submitForm();
                  })
                },
            padding: EdgeInsets.all(12.0),
            text: Text('Submit', style: TextStyle(color: Colors.white))),
      ]),
    );
  }

  @override
  onFailed(Exception onError) {
    Utility.showToast(Constants.SOMETHING_WENT, context);
  }

  @override
  onSuccess(String data, PageName pageName) {
    Utility.dismissProgress(context);
    _prayerData = prayerDataFromJson(data);
    if (_prayerData.status == 'success') {
      _controllerFullName.text = "";
      _controllerEmail.text = "";
      _controllerPersonPhone.text = "";
      _controllerMessage.text = "";
      Utility.showAlertDialog(
          context, Constants.THANK_YOU_TEXT, 'Thank You!', "Dismiss");
    } else {
      Utility.showToast(Constants.SOMETHING_WENT, context);
    }
  }

  bool validViews() {
    var isValid = true;
    _validateName = true;
    _validateEmail = true;
    _validatePhone = true;
    _validateMessage = true;

    if (_controllerFullName.text.isEmpty) {
      _validateName = false;
      isValid = false;
    } else if (_controllerEmail.text.isEmpty) {
      _validateEmail = false;
      isValid = false;
    } else if (_controllerPersonPhone.text.isEmpty) {
      _validatePhone = false;
      isValid = false;
    } else if (_controllerMessage.text.isEmpty) {
      _validateMessage = false;
      isValid = false;
    }
    return isValid;
  }

  _submitForm() {
    if (validViews()) {
      var url = "firstName=" +
          _controllerFullName.text.trim() +
          "&email=" +
          _controllerEmail.text.trim() +
          "&contact_no=" +
          _controllerPersonPhone.text.trim() +
          "&state=%20&city=%20&country=%20&message=" +
          _controllerMessage.text.trim() +
          "&time=%20";
      Utility.check().then((internet) {
        if (internet != null && internet) {
          Utility.showProgressDialog(Constants.PLEASE_WAIT, context);
          _presenter.makeGetCall(
              Constants.PRAYER_API + url, PageName.WORD_EXPLOSION);
        } else {
          Utility.showToast(Constants.NO_INTERNET_FOUND, context);
        }
      });
    }
  }
}
