import 'dart:io';

import 'package:church_app/src/screens/old_dev_non_bloc/about_me.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/custom_data_load.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/message_screen.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/enum_pages.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/webview_screen.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

//import 'package:flutter_launch/flutter_launch.dart';
import 'package:share/share.dart';

class MorePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MorePageState();
  }
}

class _MorePageState extends State<MorePage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView(
      children: <Widget>[
        Container(
            height: 260,
            child: Image.asset('images/rbn_logo.png', fit: BoxFit.fill)),
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Text(
            'Basics',
            style: TextStyle(
                color: Colors.green, fontSize: 20.0, letterSpacing: 2),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Divider(
            height: 1,
            color: Colors.grey,
          ),
        ),
        InkWell(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.message,
                  color: Colors.lightBlue,
                  size: 25.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Text('Messges'),
                )
              ],
            ),
          ),
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => MessageListPage()));
          },
        ),
        InkWell(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.announcement,
                  color: Colors.lightBlue,
                  size: 25.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Text('Letter of CEO'),
                )
              ],
            ),
          ),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => CustomDataLoadPage(
                        pageName: PageName.LETTER_FROM_CEO)));
          },
        ),
        InkWell(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.group_work,
                  color: Colors.lightBlue,
                  size: 25.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Text('Our Mission'),
                )
              ],
            ),
          ),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        CustomDataLoadPage(pageName: PageName.MISSION)));
          },
        ),
        InkWell(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.visibility,
                  color: Colors.lightBlue,
                  size: 25.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Text('Our Vision'),
                )
              ],
            ),
          ),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        CustomDataLoadPage(pageName: PageName.VISION)));
          },
        ),
        InkWell(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.markunread_mailbox,
                  color: Colors.lightBlue,
                  size: 25.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Text('Statement of Faith'),
                )
              ],
            ),
          ),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => CustomDataLoadPage(
                        pageName: PageName.STATEMENT_OF_FAITH)));
          },
        ),
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Text(
            'Like App?',
            style: TextStyle(
                color: Colors.green, fontSize: 20.0, letterSpacing: 2),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Divider(
            height: 1,
            color: Colors.grey,
          ),
        ),
        InkWell(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.thumb_up,
                  color: Colors.lightBlue,
                  size: 25.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Text('Rate us 5 start'),
                )
              ],
            ),
          ),
          onTap: () {
            if (Platform.isAndroid) {
              launch(
                  'https://play.google.com/store/apps/details?id=com.christianappdevelopers.rbn');
            } else {
              launch(
                  'https://itunes.apple.com/in/app/evans-francis/id1275222206?mt=8');
            }
          },
        ),
        InkWell(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.share,
                  color: Colors.lightBlue,
                  size: 25.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Text('Share with friends'),
                )
              ],
            ),
          ),
          onTap: () {
            if (Platform.isAndroid) {
              Share.share(
                  'Download RBN from play store \n https://play.google.com/store/apps/details?id=com.christianappdevelopers.rbn');
            } else {
              Share.share(
                  'Download RBN from App store \n https://itunes.apple.com/in/app/evans-francis/id1275222206?mt=8');
            }
          },
        ),
        InkWell(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.email,
                  color: Colors.lightBlue,
                  size: 25.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Text('Suggestions? Write us'),
                )
              ],
            ),
          ),
          onTap: () {
            launch(
                'mailto:info@rapturebroadcastingnetwork.com?subject=Suggestions&body=Hi%20Rapture%20Broadcasting,');
          },
        ),
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Text(
            'Support',
            style: TextStyle(
                color: Colors.green, fontSize: 20.0, letterSpacing: 2),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Divider(
            height: 1,
            color: Colors.grey,
          ),
        ),
        InkWell(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.call,
                  color: Colors.lightBlue,
                  size: 25.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Text('Make a call'),
                )
              ],
            ),
          ),
          onTap: () {
            launch("tel:973-39075095");
            //UrlLauncher.launch('tel:${contact.phone}');
          },
        ),
        InkWell(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.email,
                  color: Colors.lightBlue,
                  size: 25.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Text('Email'),
                )
              ],
            ),
          ),
          onTap: () {
            launch(
                'mailto:info@rapturebroadcastingnetwork.com?subject=Support&body=Hi%20Rapture%20Broadcasting,');
          },
        ),
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Text(
            'About',
            style: TextStyle(
                color: Colors.green, fontSize: 20.0, letterSpacing: 2),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Divider(
            height: 1,
            color: Colors.grey,
          ),
        ),
        InkWell(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.info,
                  color: Colors.lightBlue,
                  size: 25.0,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 12.0),
                  child: Text('About Us'),
                )
              ],
            ),
          ),
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => AboutMePage()));
          },
        ),
        InkWell(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              children: <Widget>[
                Image.asset(
                  'images/icon_facebook.png',
                  height: 25.0,
                  width: 25.0,
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 12.0),
                    child: Text('Facebook'),
                  ),
                )
              ],
            ),
          ),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => WebViewScreen(
                          title: 'Facebook',
                          url:
                              'https://m.facebook.com/rapturebroadcastingnetwork',
                        )));
          },
        ),
        InkWell(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              children: <Widget>[
                Image.asset(
                  'images/icon_web.png',
                  height: 25.0,
                  width: 25.0,
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 12.0),
                    child: Text('Website'),
                  ),
                )
              ],
            ),
          ),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => WebViewScreen(
                          title: 'Rapture Broadcasting Network',
                          url: 'http://www.rapturebroadcastingnetwork.com',
                        )));
          },
        ),
        InkWell(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              children: <Widget>[
                Image.asset(
                  'images/icon_youtube.png',
                  height: 25.0,
                  width: 25.0,
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 12.0),
                    child: Text('YouTube'),
                  ),
                )
              ],
            ),
          ),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => WebViewScreen(
                          title: 'YouTube',
                          url:
                              'https://www.youtube.com/rapturebroadcastingnetwork',
                        )));
          },
        ),
        InkWell(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.create,
                  color: Colors.lightBlue,
                  size: 25.0,
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 12.0),
                    child: Text('Created By'),
                  ),
                )
              ],
            ),
          ),
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => WebViewScreen(
                          title: 'Created By',
                          url: 'https://christianappdevelopers.com/',
                        )));
          },
        ),
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Divider(
            height: 1,
            color: Colors.grey,
          ),
        ),
        Center(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Text('Version 1.0'),
          ),
        ),
      ],
    );
  }
}
