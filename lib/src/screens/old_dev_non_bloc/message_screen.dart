import 'dart:async';
import 'package:church_app/src/models/message_data.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/message_details.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/presenters/common_presenter.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/constants.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/enum_pages.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/utility.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/views/common_view.dart';
import 'package:flutter/material.dart';

class MessageListPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MessagePageState();
  }
}

///State for VideoShowCase Class

class _MessagePageState extends State<MessageListPage> implements View {
  Presenter _presenter;
  List<MessageData> _dataList;

  _MessagePageState() {
    _presenter = new Presenter(this);
    _dataList = new List<MessageData>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Messages'),
      ),
      body: ListView.builder(
        itemCount: _dataList.length,
        itemBuilder: (BuildContext context, int index) {
          return _listViewItem(index);
        },
      ),
    );
  }

  ///Call the API here
  @override
  void initState() {
    super.initState();
    setState(() {
      Utility.check().then((internet) {
        if (internet != null && internet) {
          Future.delayed(Duration.zero,
              () => Utility.showProgressDialog(Constants.PLEASE_WAIT, context));
          _presenter.makeGetCall(Constants.MESSAGE_API, PageName.LIVE_VIDEO);
        } else {
          Utility.showToast(Constants.NO_INTERNET_FOUND, context);
        }
      });
    });
  }

  ///ListView Item layout
  Widget _listViewItem(int index) {
    return InkWell(
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  'images/rbn_logo.png',
                  height: 80,
                  width: 80,
                ),
                Expanded(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Text(
                        _dataList[index].name,
                        maxLines: 1,
                        style: TextStyle(
                            color: Colors.lightGreen,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Text(
                        _dataList[index].content.replaceAll('&quot;', ''),
                        maxLines: 1,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Text(
                        _dataList[index].messageDate.toString() +
                            " " +
                            _dataList[index].messageTime,
                        style: TextStyle(fontSize: 10.0),
                      ),
                    ),
                  ],
                ))
              ],
            ),
            Divider(
              color: Colors.lightGreen,
            )
          ],
        ),
      ),
      onTap: () {
        var content = _dataList[index].content.replaceAll('&quot;', '');
        content = content.replaceAll('&#39;', '');
        content = content.replaceAll('&nbsp;', '');
        _dataList[index].content = content;
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => MessageDetails(data: _dataList[index])));
      },
    );
  }

  @override
  onFailed(Exception onError) {
    Utility.dismissProgress(context);
    Utility.showToast(Constants.SOMETHING_WENT, context);
  }

  @override
  onSuccess(String data, PageName pageName) {
    Utility.dismissProgress(context);

    setState(() {
      try {
        _dataList = messageDataFromJson(data);
      } on TypeError {
        Utility.showToast(Constants.NO_DATA, context);
      }
    });
  }
}
