import 'package:cached_network_image/cached_network_image.dart';
import 'package:church_app/src/models/message_data.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/constants.dart';
import 'package:flutter/material.dart';

class MessageDetails extends StatelessWidget {
  final MessageData data;

  const MessageDetails({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Messages Details'),
      ),
      body: ListView(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: 220,
                child: CachedNetworkImage(
                  imageUrl: Constants.MESSAGE_THUMBNAIL + data.image,
                  placeholder: (context, url) => Image.asset(
                      'images/ic_placeholder.png',
                      height: 220,
                      fit: BoxFit.fill,
                      width: MediaQuery.of(context).size.width),
                  errorWidget: (context, url, error) => Image.asset(
                      'images/ic_placeholder.png',
                      height: 220,
                      fit: BoxFit.fill,
                      width: MediaQuery.of(context).size.width),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: Text(
                  data.name,
                  maxLines: 1,
                  style: TextStyle(
                      color: Colors.lightGreen, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: Text(
                  data.content,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: Text(
                  data.messageDate.toString() + " " + data.messageTime,
                  style: TextStyle(fontSize: 10.0),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
