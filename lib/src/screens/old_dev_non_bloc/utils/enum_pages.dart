///This enum is using identification for pages
///during the API call
enum PageName {
  LIVE_VIDEO,
  MUSIC,
  MISSION,
  STATEMENT_OF_FAITH,
  WORD_EXPLOSION,
  MIRACLE,
  EYE_TO_EYE,
  MELODIES,
  ROCK_IF_LIFE,
  LETTER_FROM_CEO,
  VISION,
}
