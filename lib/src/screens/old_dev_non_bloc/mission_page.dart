import 'dart:async';
import 'package:church_app/src/models/mission_data.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/presenters/common_presenter.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/constants.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/enum_pages.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/utility.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/views/common_view.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class MissionPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MissionPageState();
  }
}

///State for VideoShowCase Class

class _MissionPageState extends State<MissionPage> implements View {
  Presenter _presenter;
  List<MissionData> _dataList;

  _MissionPageState() {
    _presenter = new Presenter(this);
    _dataList = new List<MissionData>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Missions'),
      ),
      body: ListView.builder(
        itemCount: _dataList.length,
        itemBuilder: (BuildContext context, int index) {
          return _listViewItem(index);
        },
      ),
    );
  }

  ///Call the API here
  @override
  void initState() {
    super.initState();
    setState(() {
      if (Utility.isConnected() != null) {
        Future.delayed(Duration.zero,
            () => Utility.showProgressDialog(Constants.PLEASE_WAIT, context));
        _presenter.makeGetCall(Constants.MISSIONS_API, PageName.MISSION);
      } else {
        Utility.showToast(Constants.NO_INTERNET_FOUND, context);
      }
    });
  }

  ///ListView Item layout
  Widget _listViewItem(int index) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          width: MediaQuery.of(context).size.width,
          height: 200,
          child: CachedNetworkImage(
            fit: BoxFit.fill,
            imageUrl: Constants.MISSION_THUMBNAIL + _dataList[index].image,
            placeholder: (context, url) => Image.asset(
                'images/ic_placeholder.png',
                height: 200,
                fit: BoxFit.fill,
                width: MediaQuery.of(context).size.width),
            errorWidget: (context, url, error) => Image.asset(
                'images/ic_placeholder.png',
                height: 200,
                fit: BoxFit.fill,
                width: MediaQuery.of(context).size.width),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8.0, left: 8.0, bottom: 8.0),
          child: Text(
            _dataList[index].name,
            maxLines: 8,
            style: TextStyle(
                fontSize: 15,
                color: Colors.black,
                fontWeight: FontWeight.bold,
                letterSpacing: 1),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8.0, bottom: 4.0),
          child: Text(
            'Event Time :' + _dataList[index].missionTime,
            style: TextStyle(fontSize: 12.0),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
          child: Text(
            'Event Date: ' + _dataList[index].missionDate,
            style: TextStyle(fontSize: 12.0),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8.0, bottom: 8.0),
          child: Text(
            'Details: ' + _dataList[index].content,
            style: TextStyle(fontSize: 12.0),
          ),
        ),
      ],
    );
  }

  @override
  onFailed(Exception onError) {
    Utility.dismissProgress(context);
    Utility.showToast(Constants.SOMETHING_WENT, context);
  }

  @override
  onSuccess(String data, PageName pageName) {
    Utility.dismissProgress(context);

    setState(() {
      _dataList = missionDataFromJson(data);
    });
  }
}
