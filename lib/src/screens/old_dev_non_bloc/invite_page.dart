import 'package:church_app/src/models/invite_data.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/components/button_widget.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/components/text_field_widget.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/presenters/common_presenter.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/constants.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/enum_pages.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/utility.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/views/common_view.dart';
import 'package:flutter/material.dart';

class InvitePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _InvitePageState();
  }
}

class _InvitePageState extends State<InvitePage> implements View {
  final _controllerPersonPhone = TextEditingController();
  final _controllerFullName = TextEditingController();
  final _controllerEventDate = TextEditingController();
  final _controllerEventTheme = TextEditingController();
  final _controllerEmail = TextEditingController();
  var errorMessage = Constants.EMPTY_TEXT_ERROR;

  var _validateName = true;
  var _validateEmail = true;
  var _validatePhone = true;
  var _validateEventDate = true;
  var _validateEventTheme = true;

  Presenter _presenter;
  InviteData _inviteData;

  _InvitePageState() {
    _presenter = new Presenter(this);
    _inviteData = new InviteData();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: ListView(children: <Widget>[
        EditTextWidget(
          errorValidation: _validateName,
          controller: _controllerFullName,
          errorText: errorMessage,
          labelText: 'Full Name*',
          keyboardType: TextInputType.text,
          isObscureText: false,
        ),
        EditTextWidget(
          errorValidation: _validateEmail,
          controller: _controllerEmail,
          errorText: Constants.EMPTY_TEXT_ERROR,
          labelText: 'Email*',
          keyboardType: TextInputType.emailAddress,
          isObscureText: false,
        ),
        EditTextWidget(
          errorValidation: _validatePhone,
          controller: _controllerPersonPhone,
          errorText: Constants.EMPTY_TEXT_ERROR,
          labelText: 'Contact',
          isObscureText: false,
          keyboardType: TextInputType.number,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: Container(
            height: 40,
            width: MediaQuery.of(context).size.width,
            color: Colors.grey,
            child: Center(
              child: Text(
                'Event Details',
                style: TextStyle(color: Colors.deepPurple),
              ),
            ),
          ),
        ),
        EditTextWidget(
          errorValidation: _validateEventTheme,
          controller: _controllerEventTheme,
          errorText: Constants.EMPTY_TEXT_ERROR,
          labelText: 'Event Theme',
          isObscureText: false,
          keyboardType: TextInputType.text,
        ),
        EditTextWidget(
          errorValidation: _validateEventDate,
          controller: _controllerEventDate,
          errorText: Constants.EMPTY_TEXT_ERROR,
          labelText: 'Event Date',
          isObscureText: false,
          keyboardType: TextInputType.datetime,
        ),
        ButtonWidget(
            onPressed: () => {
                  setState(() {
                    _submitForm();
                  })
                },
            padding: EdgeInsets.all(12.0),
            text: Text('Submit', style: TextStyle(color: Colors.white))),
      ]),
    );
  }

  bool validViews() {
    var isValid = true;
    _validateName = true;
    _validateEmail = true;
    _validatePhone = true;
    _validateEventDate = true;
    _validateEventTheme = true;

    if (_controllerFullName.text.isEmpty) {
      _validateName = false;
      isValid = false;
    } else if (_controllerEmail.text.isEmpty) {
      _validateEmail = false;
      isValid = false;
    } else if (_controllerPersonPhone.text.isEmpty) {
      _validatePhone = false;
      isValid = false;
    } else if (_controllerEventTheme.text.isEmpty) {
      _validateEventTheme = false;
      isValid = false;
    } else if (_controllerEventDate.text.isEmpty) {
      _validateEventDate = false;
      isValid = false;
    }
    return isValid;
  }

  _submitForm() {
    if (validViews()) {
      var url = "name=" +
          _controllerFullName.text.trim() +
          "&email=" +
          _controllerEmail.text.trim() +
          "&address=%20&contact_no=" +
          _controllerPersonPhone.text.trim() +
          "&country=%20&state=%20&city=%20&zipcode=%20&website=%20&contactPerson=%20&contactPersonphone=%20&overseer=%20&eventTitle=%20&eventTheme=" +
          _controllerEventTheme.text.trim() +
          "&dateOfevent=" +
          _controllerEventDate.text.trim() +
          "&estimatedAttendance=%20";
      Utility.showProgressDialog(Constants.PLEASE_WAIT, context);
      _presenter.makeGetCall(Constants.INVITE_API + url, PageName.LIVE_VIDEO);
    }
  }

  @override
  onFailed(Exception onError) {
    Utility.showToast(Constants.SOMETHING_WENT, context);
  }

  @override
  onSuccess(String data, PageName pageName) {
    Utility.dismissProgress(context);
    _inviteData = inviteDataFromJson(data);
    print('Invite Response is ' + _inviteData.status);
    if (_inviteData.status == 'success') {
      _controllerFullName.text = "";
      _controllerEmail.text = "";
      _controllerPersonPhone.text = "";
      _controllerEventDate.text = "";
      _controllerEventTheme.text = "";
      Utility.showAlertDialog(
          context, Constants.THANK_YOU_TEXT, 'Thank You!', "Dismiss");
    }
  }
}
