import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:church_app/src/models/gallery_data.dart';
import 'package:church_app/src/models/gallery_list_data.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/presenters/common_presenter.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/constants.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/enum_pages.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/utils/utility.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/views/common_view.dart';
import 'package:flutter/material.dart';

class GalleryImagesPage extends StatefulWidget {
  final GalleryData data;

  const GalleryImagesPage({Key key, this.data}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _GalleryState(data);
  }
}

class _GalleryState extends State<GalleryImagesPage> implements View {
  GalleryData _galleryAlbumData;
  List<GalleryListData> _listData;
  Presenter _presenter;

  _GalleryState(GalleryData data) {
    _galleryAlbumData = data;
    _presenter = new Presenter(this);
    _listData = new List<GalleryListData>();
  }

  ///Call the API here
  @override
  void initState() {
    super.initState();
    setState(() {
      if (Utility.isConnected() != null) {
        Future.delayed(Duration.zero,
            () => Utility.showProgressDialog(Constants.PLEASE_WAIT, context));
        _presenter.makeGetCall(
            Constants.GALLERY_LIST_API + _galleryAlbumData.id.toString(),
            PageName.MUSIC);
      } else {
        Utility.showToast(Constants.NO_INTERNET_FOUND, context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_galleryAlbumData.name),
      ),
      body: PageView.builder(
        itemBuilder: (context, position) {
          return CachedNetworkImage(
            imageUrl:
                Constants.GALLERY_ALBUM_THUMBNAIL + _listData[position].image,
            placeholder: (context, url) => Image.asset(
              'images/ic_placeholder.png',
              fit: BoxFit.fill,
            ),
            errorWidget: (context, url, error) => Image.asset(
              'images/ic_placeholder.png',
              fit: BoxFit.fill,
            ),
            height: double.infinity,
            width: double.infinity,
            alignment: Alignment.center,
          );
        },
        itemCount: _listData.length, // Can be null
      ),
    );
  }

  ///ListView Item layout
  Widget _listViewItem(int index) {
    return InkWell(
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: 56,
                height: 56,
                child: CachedNetworkImage(
                  fit: BoxFit.fill,
                  imageUrl: Constants.GALLERY_ALBUM_THUMBNAIL +
                      _listData[index].image,
                  placeholder: (context, url) => Image.asset(
                      'images/ic_placeholder.png',
                      height: 56,
                      fit: BoxFit.fill,
                      width: 56),
                  errorWidget: (context, url, error) => Image.asset(
                      'images/ic_placeholder.png',
                      height: 56,
                      fit: BoxFit.fill,
                      width: 56),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0, left: 8.0),
                child: Text(
                  _listData[index].name,
                  maxLines: 2,
                  style: TextStyle(letterSpacing: 1),
                ),
              ),
            ],
          ),
          Divider(
            color: Color.fromRGBO(23, 7, 56, .3),
          )
        ],
      ),
      onTap: () {
        /*Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    BlogDetails(blogData: _blogDataList[index])));*/
      },
    );
  }

  @override
  onFailed(Exception onError) {
    Utility.dismissProgress(context);
    Utility.showToast(Constants.SOMETHING_WENT, context);
  }

  @override
  onSuccess(String data, PageName pageName) {
    Utility.dismissProgress(context);
    setState(() {
      try {
        _listData = galleryListDataFromJson(data);
      } on TypeError {
        Utility.showToast(Constants.NO_DATA, context);
      }
    });
  }
}
