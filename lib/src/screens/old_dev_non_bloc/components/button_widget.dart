import 'package:flutter/material.dart';

class ButtonWidget extends StatelessWidget {
  ButtonWidget({this.text, this.padding, this.onPressed});

  ///Widget for button text
  final Text text;

  ///Padding Widget
  final EdgeInsets padding;

  ///On Pressed Event
  final GestureTapCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding != null
          ? padding
          : EdgeInsets.only(left: 0, right: 0, top: 0, bottom: 0),
      child: RaisedButton(
          child: Container(
            child: Center(child: text),
          ),
          onPressed: onPressed,
          color: Colors.green,
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(20.0))),
    );
  }
}
