import 'package:church_app/src/screens/old_dev_non_bloc/gallery_show_case.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/home_page.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/more_pages.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/music_album_page.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/prayer_page.dart';
import 'package:flutter/material.dart';

void main() => runApp(new HomeScreen());

class HomeScreen extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeState();
  }
}

class _HomeState extends State<Home> {
  int _currentIndex = 0;
  String _titleName = "Home";
  final List<Widget> _children = [
    HomePage(),
    MusicAlbumPage(),
    PrayerPage(),
    GalleryShowCase(),
    MorePage(),
  ];

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: new Text(_titleName),
      ),
      /*floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: new FloatingActionButton(

        onPressed:(){ HomePage(); },
        tooltip: 'Increment',
        child: new Icon(Icons.add),
      ),*/
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.home),
            title: new Text('Home'),
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.video_library), title: Text('Audio')),
          BottomNavigationBarItem(
              icon: Icon(Icons.pan_tool), title: Text("Prayer")),
          BottomNavigationBarItem(
              icon: Icon(Icons.photo_library), title: Text('Gallery')),
          BottomNavigationBarItem(icon: Icon(Icons.more), title: Text('More')),
        ],
      ),
    );
  }

  ///This method will called on tap of bottom menu
  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
      if (index == 0) {
        _titleName = "Home";
      } else if (index == 1) {
        _titleName = "Audio";
      } else if (index == 2) {
        _titleName = "Prayer Request";
      } else if (index == 3) {
        _titleName = "Gallery";
      } else if (index == 4) {
        _titleName = "More";
      }
    });
  }
}
