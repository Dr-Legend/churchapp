part of 'testimony_bloc.dart';

@immutable
abstract class TestimonyEvent {}

@immutable
class LoadTestimonyEvent extends TestimonyEvent {}
