import 'package:church_app/src/screens/testimony/testimony_bloc.dart';
import 'package:church_app/src/utils/constants.dart';
import 'package:church_app/src/widgets/HtmlContentScreen.dart';
import 'package:church_app/src/widgets/list_tile_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:church_app/src/screens/blog/blog_state.dart';
import 'package:church_app/src/widgets/html_view.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:church_app/src/church_app_colors.dart';
import 'package:church_app/src/models/blog_data.dart';
import 'package:church_app/src/screens/blog/blog_bloc.dart';

class Testimony extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final double _radius = 25.0;
    final double _screenHeight = MediaQuery.of(context).size.height;
    final double _carouselSize = _screenHeight / 2.1;
    return SafeArea(
      child: Scaffold(
        body: BlocBuilder<TestimonyBloc, TestimonyState>(
          builder: (BuildContext context, TestimonyState state) {
            if (state is LoadingTestimonyState) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is ErrorTestimonyState) {
              return Center(
                child: Text("Unable to fetch blogs please try again later.."),
              );
            }
            if (state is LoadedTestimonyState) {
              return Container(
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: ListView.builder(
                        shrinkWrap: false,
                        itemCount: state.testimonyData.length ?? 0,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => HtmlContentScreenBasic(
                                        name: state.testimonyData[index].name,
                                        content:
                                            state.testimonyData[index].content,
                                        image:
                                            '${Constants.TESTIMONY_Thumb}${state.testimonyData[index].image}',
                                        title: 'Testimonies',
                                        date: state
                                            .testimonyData[index].testimonyDate,
                                      )));
                            },
                            child: ListTileCard(
                              name: state.testimonyData[index].name,
                              imageUrl:
                                  '${Constants.TESTIMONY_Thumb}${state.testimonyData[index].image}',
                              date: state.testimonyData[index].testimonyDate,
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              );
            }
            return Container(
              child: Center(
                child: Text("you shoudn't see this"),
              ),
            );
          },
        ),
      ),
    );
  }
}
