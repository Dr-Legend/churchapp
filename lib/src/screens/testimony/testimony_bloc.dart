import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:church_app/src/api/api.dart';
import 'package:church_app/src/models/testimony.dart';
import 'package:church_app/src/screens/testimony/testimony_screen.dart';
import 'package:church_app/src/utils/constants.dart';
import 'package:meta/meta.dart';

part 'testimony_event.dart';

part 'testimony_state.dart';

class TestimonyBloc extends Bloc<TestimonyEvent, TestimonyState> {
  final ApiMethods api = ApiMethods();

  @override
  TestimonyState get initialState => InitialTestimonyState();

  @override
  Stream<TestimonyState> mapEventToState(TestimonyEvent event) async* {
    if (event is LoadTestimonyEvent) {
      yield LoadingTestimonyState();
      try {
        String response = await api.requestInGet(Constants.TESTIMONY_API);
        List<TestimonyData> testimonies = testimonyDataFromJson(response);
        yield LoadedTestimonyState(testimonies);
      } catch (e) {
        yield ErrorTestimonyState(e);
      }
    }
  }
}
