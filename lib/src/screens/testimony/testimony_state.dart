part of 'testimony_bloc.dart';

@immutable
abstract class TestimonyState {}

class InitialTestimonyState extends TestimonyState {}

class LoadingTestimonyState extends TestimonyState {}

class LoadedTestimonyState extends TestimonyState {
  final List<TestimonyData> testimonyData;

  LoadedTestimonyState(this.testimonyData);
}

class ErrorTestimonyState extends TestimonyState {
  final String error;

  ErrorTestimonyState(this.error);
}
