import 'package:meta/meta.dart';

@immutable
abstract class PdfScreenEvent {}

class LoadPdfScreenEvent extends PdfScreenEvent {}
