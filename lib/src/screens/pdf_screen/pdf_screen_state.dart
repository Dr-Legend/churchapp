import 'package:church_app/src/models/pdf_data.dart';
import 'package:meta/meta.dart';

@immutable
abstract class PdfScreenState {}

class InitialPdfScreenState extends PdfScreenState {}

class LoadingPdfScreenState extends PdfScreenState {}

class ErrorPdfScreenState extends PdfScreenState {
  final String error;

  ErrorPdfScreenState(this.error);
}

class PdfReadyScreenState extends PdfScreenState {
  final List<PDF> pdfNotes;

  PdfReadyScreenState({this.pdfNotes});
}
