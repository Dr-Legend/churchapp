import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:church_app/src/api/api.dart';
import 'package:church_app/src/models/pdf_data.dart';
import 'package:church_app/src/screens/pdf_screen/pdf_screen_event.dart';
import 'package:church_app/src/screens/pdf_screen/pdf_screen_state.dart';
import 'package:church_app/src/utils/constants.dart';

class PdfScreenBloc extends Bloc<PdfScreenEvent, PdfScreenState> {
  final ApiMethods api = ApiMethods();
  @override
  PdfScreenState get initialState => InitialPdfScreenState();

  @override
  Stream<PdfScreenState> mapEventToState(PdfScreenEvent event) async* {
    if (event is LoadPdfScreenEvent) {
      yield LoadingPdfScreenState();
      try {
        String response = await api.requestInGet(Constants.PDF_Api);
        List<PDF> pdfNotes = pdfDataFromJson(response);
        yield PdfReadyScreenState(pdfNotes: pdfNotes);
      } catch (e) {
        yield ErrorPdfScreenState(e);
      }
    }
  }
}
