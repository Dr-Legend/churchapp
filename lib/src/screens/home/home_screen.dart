import 'package:banner_view/banner_view.dart';
import 'package:church_app/src/screens/blog/blog_bloc.dart';
import 'package:church_app/src/screens/blog/blog_event.dart';
import 'package:church_app/src/screens/blog/blog_screen.dart';
import 'package:church_app/src/screens/home/home_bloc.dart';
import 'package:church_app/src/screens/home/home_event.dart';
import 'package:church_app/src/church_app_colors.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/blog_details.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/gallery_show_case.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/invite_page.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/music_album_page.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/prayer_request.dart';
import 'package:church_app/src/screens/old_dev_non_bloc/video_page.dart';
import 'package:church_app/src/screens/pdf_screen/pdf_screen.dart';
import 'package:church_app/src/screens/pdf_screen/pdf_screen_bloc.dart';
import 'package:church_app/src/screens/pdf_screen/pdf_screen_event.dart';
import 'package:church_app/src/screens/testimony/testimony_bloc.dart';
import 'package:church_app/src/screens/testimony/testimony_screen.dart';
import 'package:church_app/src/widgets/main_drawer.dart';
import 'package:church_app/src/widgets/material_buttons.dart' as MaterialButton;
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  PageController _pageController;
  ScrollController _scrollController;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  Map<String, Map<Image, Widget>> menuItems = <String, Map<Image, Widget>>{
    "Live": {
      Image.asset(
        "images/icon_live.png",
        fit: BoxFit.fill,
        scale: 5,
      ): Container()
    },
    "Blog": {
      Image.asset(
        "images/icon_blog.png",
        scale: 5,
        fit: BoxFit.contain,
      ): Blog()
    },
    "Music": {
      Image.asset(
        "images/icon_mp3.png",
        scale: 5,
        fit: BoxFit.contain,
      ): MusicAlbumPage()
    },
    "Video": {
      Image.asset(
        "images/icon_video.png",
        scale: 5,
        fit: BoxFit.contain,
      ): VideoShowCase()
    },
    "Gallery": {
      Image.asset(
        "images/icon_gallery.png",
        scale: 5,
        fit: BoxFit.contain,
      ): GalleryShowCase()
    },
    "Prayer": {
      Image.asset(
        "images/icon_prayer_request.png",
        scale: 5,
        fit: BoxFit.contain,
      ): PrayerRequestScreen()
    },
    "Pillar of Fire": {
      Image.asset(
        "images/icon_pillar_of_fire.png",
        scale: 5,
        fit: BoxFit.contain,
      ): Container()
    },
    "Pillar of Cloud": {
      Image.asset(
        "images/icon_pillar_of_cloud.png",
        scale: 5,
        fit: BoxFit.contain,
      ): Container()
    },
    "Testimonies": {
      Image.asset(
        "images/icon_testimonies.png",
        scale: 5,
        fit: BoxFit.contain,
      ): Testimony(),
    },
    "PDF Notes": {
      Image.asset(
        "images/icon_pdf1.png",
        scale: 8,
        fit: BoxFit.contain,
      ): PdfScreen(),
    },
    "Invite": {
      Image.asset(
        "images/icon_invite.png",
        scale: 5,
        fit: BoxFit.contain,
      ): InvitePage()
    },
  };

  @override
  void initState() {
    _scrollController = ScrollController();
    _pageController = PageController(
      initialPage: 0,
      keepPage: true,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    HomeBloc _homeBloc = BlocProvider.of<HomeBloc>(context);

    bool themeIsLight = DynamicTheme.of(context).brightness == Brightness.light;
    var brightness = DynamicTheme.of(context).brightness;
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        extendBodyBehindAppBar: true,
        drawer: MainDrawer(),
        appBar: AppBar(
          title: Text(
            "ChurchApp",
          ),
        ),
        body: PageView(
          controller: _pageController,
          children: <Widget>[
            Home(menuItems: menuItems),
          ],
        ),
//        floatingActionButton: FloatingActionButton(
//          onPressed: _incrementCounter,
//          tooltip: 'Increment',
//          child: Icon(Icons.add),
//        ),
      ),
    );
  }
}

class Home extends StatelessWidget {
  const Home({
    Key key,
    @required this.menuItems,
  }) : super(key: key);

  final Map<String, Map<Image, Widget>> menuItems;

  onTap(
      {@required Widget widget,
      @required BuildContext context,
      @required String title}) {
    if (title == "Blog") {
      BlocProvider.of<BlogBloc>(context).dispatch(LoadBlogEvent());
    } else if (title == 'Testimonies') {
      BlocProvider.of<TestimonyBloc>(context).dispatch(LoadTestimonyEvent());
    } else if (title == 'PDF Notes') {
      BlocProvider.of<PdfScreenBloc>(context).dispatch(LoadPdfScreenEvent());
    }
    print("On tap called");
    return Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => title != "Blog"
            ? Scaffold(
                appBar: AppBar(
                  title: Text(title),
                ),
                body: widget)
            : widget));
  }

  @override
  Widget build(BuildContext context) {
    var brightness = DynamicTheme.of(context).brightness;
    return Column(
      children: <Widget>[
        Container(
          height: 300.0,
          child: BannerView(
            [
              Container(
                  child: Image.asset('images/slider1.png', fit: BoxFit.fill)),
              Container(
                  child: Image.asset('images/slider2.png', fit: BoxFit.fill)),
              Container(
                  child: Image.asset('images/slider3.png', fit: BoxFit.fill)),
              Container(
                  child: Image.asset('images/slider4.png', fit: BoxFit.fill)),
              Container(
                  child: Image.asset('images/slider5.png', fit: BoxFit.fill)),
              Container(
                  child: Image.asset('images/slider6.png', fit: BoxFit.fill)),
              Container(
                  child: Image.asset('images/slider7.png', fit: BoxFit.fill)),
            ],
            intervalDuration: Duration(
                days: 0,
                hours: 0,
                microseconds: 0,
                milliseconds: 0,
                minutes: 0,
                seconds: 3),
            log: false,
          ),
        ),
        Flexible(
          child: Stack(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 0),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    color: ChurchAppColors.scaffoldBackground,
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Container(
                  color: brightness == Brightness.dark
                      ? Colors.black54
                      : Colors.white,
                  child: GridView.builder(
                    padding: EdgeInsets.all(12),
                    shrinkWrap: true,
                    itemCount: menuItems.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3),
                    itemBuilder: (BuildContext context, int index) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: MaterialButton.MaterialButton(
//                    shadowColor: Color(0xFF64E1aa),
                          shadowColor: Colors.black,
                          highlightColor: Colors.white,

                          splashColor: ChurchAppColors.lightYellow,
                          elevation: 10,
                          child: Column(
                            children: <Widget>[
                              menuItems.values
                                  .elementAt(index)
                                  .keys
                                  .elementAt(0),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                menuItems.keys.elementAt(index),
                                style: TextStyle(
                                    fontSize: 12,
                                    color: Theme.of(context)
                                        .primaryTextTheme
                                        .title
                                        .color),
                              ),
                            ],
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30)),
                          color: Theme.of(context).cardColor,

                          onPressed: () => onTap(
                              title: menuItems.keys
                                      .elementAt(index)
                                      .contains("Invite")
                                  ? "Invite Evans Francis"
                                  : menuItems.keys.elementAt(index),
                              widget: menuItems.values
                                  .elementAt(index)
                                  .values
                                  .elementAt(0),
                              context: context),
                        ),
                      );
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class AngleClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    // Since the wave goes vertically lower than bottom left starting point,
    // we'll have to make this point a little higher.
    path.lineTo(0.0, size.height);

    // The bottom right point also isn't at the same level as its left counterpart,
    // so we'll adjust that one too.
    path.lineTo(size.width, size.height - 100);
    path.lineTo(size.height, 0);
    // Draw a straight line from current point to the top right corner.
    path.lineTo(0.0, size.width - size.height + 50);
    path.lineTo(0.0, size.height);

    //close the path
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    // TODO: implement shouldReclip
    return true;
  }
}
