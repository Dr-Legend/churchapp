import 'package:flutter/material.dart';

@immutable
abstract class HomeEvent {}

class ChangeBottomNavigationBarIndex extends HomeEvent {
  final int index;

  ChangeBottomNavigationBarIndex(this.index);
}
