import 'package:flutter/material.dart';

@immutable
class HomeState {
  final int bottomNavigationBarIndex;

  const HomeState({
    @required this.bottomNavigationBarIndex,
  });

  HomeState copyWith({
    int bottomNavigationBarIndex,
  }) {
    return HomeState(
      bottomNavigationBarIndex:
          bottomNavigationBarIndex ?? this.bottomNavigationBarIndex,
    );
  }

  Map<String, dynamic> toJson() => {
        'bottomNavigationBarIndex': bottomNavigationBarIndex,
      };

  factory HomeState.fromJson(Map<String, dynamic> json) {
    return HomeState(
        bottomNavigationBarIndex: json['bottomNavigationBarIndex'] ?? 0);
  }
}

class InitialHomeState extends HomeState {
  const InitialHomeState()
      : super(
          bottomNavigationBarIndex: 0,
        );
}
