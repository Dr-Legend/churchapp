import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:church_app/src/screens/home/home_state.dart';
import 'package:church_app/src/screens/home/home_event.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  @override
  HomeState get initialState => InitialHomeState();

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is ChangeBottomNavigationBarIndex) {
      yield currentState.copyWith(bottomNavigationBarIndex: event.index);
      return;
    }
  }
}
