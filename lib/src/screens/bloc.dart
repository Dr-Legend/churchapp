import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';

GetIt getIt = GetIt.instance;

class ChurchAppBlocDelegate extends HydratedBlocDelegate {
  ChurchAppBlocDelegate(HydratedStorage storage) : super(storage);

  @override
  void onEvent(Bloc bloc, Object event) {
    debugPrint("$event called for $bloc");
    super.onEvent(bloc, event);
  }

  Future<BlocDelegate> build() => HydratedBlocDelegate.build();
}
