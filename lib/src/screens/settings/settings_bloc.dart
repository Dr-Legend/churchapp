import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:church_app/src/screens/settings/settings_event.dart';
import 'package:church_app/src/convert.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:church_app/src/screens/settings/settings_state.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  @override
  SettingsState get initialState => InitialSettingsState();

  @override
  Stream<SettingsState> mapEventToState(SettingsEvent event) async* {
    if (event is ToggleThemeEvent) {
      final newBrightness = _toggleTheme(event.context);
      writeToPrefs(brightness: newBrightness);
      yield currentState.copyWith(brightness: newBrightness);
      return;
    }
  }

  Brightness _toggleTheme(BuildContext context) {
    Brightness newBrightness = Theme.of(context).brightness == Brightness.dark
        ? Brightness.light
        : Brightness.dark;

    DynamicTheme.of(context).setBrightness(newBrightness);
    return newBrightness;
  }

  Future<SettingsState> loadFromPrefs(SettingsState state) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String brightnessString = prefs.get('brightness');
    Brightness brightness = convertStringToBrightness(brightnessString);

    return state.copyWith(
      brightness: brightness,
    );
  }

  void writeToPrefs({
    Brightness brightness,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (brightness != null) {
      prefs.setString('filterType', convertBrightnessToString(brightness));
    }
  }
}
