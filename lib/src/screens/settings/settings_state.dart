import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

@immutable
class SettingsState {
  final Brightness brightness;

  const SettingsState({
    @required this.brightness,
  }) : assert(brightness != null);

  SettingsState copyWith({
    Brightness brightness,
  }) {
    return SettingsState(
      brightness: brightness ?? this.brightness,
    );
  }
}

class InitialSettingsState extends SettingsState {
  const InitialSettingsState()
      : super(
          brightness: Brightness.dark,
        );
}
