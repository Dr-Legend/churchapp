import 'package:flutter/material.dart';

@immutable
abstract class SettingsEvent {}

class ToggleThemeEvent extends SettingsEvent {
  final BuildContext context;

  ToggleThemeEvent({@required this.context}) : assert(context != null);
}
