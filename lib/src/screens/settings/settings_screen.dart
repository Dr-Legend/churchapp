import 'package:church_app/src/screens/settings/settings_bloc.dart';
import 'package:church_app/src/screens/settings/settings_event.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:church_app/src/screens/settings/settings_state.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';
import 'package:package_info/package_info.dart';

class SettingsScreen extends StatefulWidget {
  final SettingsState settingsState;

  const SettingsScreen({Key key, @required this.settingsState})
      : super(key: key);

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final SettingsBloc settingsBloc = BlocProvider.of<SettingsBloc>(context);
    bool themeIsLight = DynamicTheme.of(context).brightness == Brightness.light;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: Theme.of(context).canvasColor,
        title: Text(
          'ChurchApp',
          style: TextStyle(
            color: themeIsLight ? Colors.black : Colors.white,
          ),
        ),
        iconTheme: IconThemeData(
          color: themeIsLight ? Colors.black : Colors.white,
        ),
      ),
      body: Column(
        children: <Widget>[
          ListTile(
            title: Text(
              'Set Theme to ${themeIsLight ? 'Dark' : 'Light'}',
            ),
            trailing:
                Icon(themeIsLight ? Icons.brightness_3 : Icons.brightness_6),
            onTap: () {
              settingsBloc.dispatch(ToggleThemeEvent(context: context));
            },
          ),
        ],
      ),
    );
  }
}
