import 'package:flutter/material.dart' show Color, Colors;

class ChurchAppColors {
  static Color darkColor = Color.fromRGBO(18, 32, 48, 1);
  static Color darkAccent = Colors.grey[800];
  static Color lightBlue = Colors.cyan;
  static Color golden = Color(0xFFD4AF37);
  static Color lightYellow = Color(0xFFFBDC73);
  static Color pinkRed = Color(0xFFF55564);
  static Color lightPinkRed = Color(0xFFF8A2A4);
  static Color navyBlue = Color(0xFF5A6571);
  static Color scaffoldBackground = Color(0xFFFFFFFF);
}
