import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

///This class has API calling methods
///
class ApiMethods {
  ///This method handles request in GET
  Future<String> requestInGet(String url) async {
    final response = await http.get(url,
        headers: {"Accept": "application/json"}).timeout(Duration(seconds: 10));
    print(response.body);
    if (response.statusCode == 200) {
      /// If server returns an OK response, return the response
      return response.body;
    } else {
      throw Exception('Failed to load data');
    }
  }

  Future<http.Response> requestInPost(String url, String tokens) async {
    final response = await http.post(url);
    if (response.statusCode == 200) {
      /// If server returns an OK response, return the response
      return response;
    } else {
      throw Exception('Failed to load data');
    }
  }
}
