import 'package:flutter/material.dart';

String convertBrightnessToString(Brightness brightness) {
  switch (brightness) {
    case Brightness.light:
      return 'light';
    case Brightness.dark:
    default:
      return 'dark';
  }
}

Brightness convertStringToBrightness(String brightnessString) {
  switch (brightnessString) {
    case 'light':
      return Brightness.light;
    case 'dark':
    default:
      return Brightness.dark;
  }
}
