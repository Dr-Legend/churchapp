// To parse this JSON data, do
//
//     final missionData = missionDataFromJson(jsonString);

import 'dart:convert';

List<MissionData> missionDataFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<MissionData>.from(
      jsonData.map((x) => MissionData.fromJson(x)));
}

String missionDataToJson(List<MissionData> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class MissionData {
  String status;
  String id;
  String name;
  String image;
  String missionTime;
  String missionDate;
  String content;

  MissionData({
    this.status,
    this.id,
    this.name,
    this.image,
    this.missionTime,
    this.missionDate,
    this.content,
  });

  factory MissionData.fromJson(Map<String, dynamic> json) => new MissionData(
        status: json["status"] == null ? null : json["status"],
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        image: json["image"] == null ? null : json["image"],
        missionTime: json["missionTime"] == null ? null : json["missionTime"],
        missionDate: json["missionDate"] == null ? null : json["missionDate"],
        content: json["content"] == null ? null : json["content"],
      );

  Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "image": image == null ? null : image,
        "missionTime": missionTime == null ? null : missionTime,
        "missionDate": missionDate == null ? null : missionDate,
        "content": content == null ? null : content,
      };
}
