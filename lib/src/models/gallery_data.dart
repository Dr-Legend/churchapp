// To parse this JSON data, do
//
//     final galleryData = galleryDataFromJson(jsonString);

import 'dart:convert';

List<GalleryData> galleryDataFromJson(String str) => new List<GalleryData>.from(json.decode(str).map((x) => GalleryData.fromJson(x)));

String galleryDataToJson(List<GalleryData> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class GalleryData {
  int totalImages;
  String status;
  int id;
  String name;
  String image;

  GalleryData({
    this.totalImages,
    this.status,
    this.id,
    this.name,
    this.image,
  });

  factory GalleryData.fromJson(Map<String, dynamic> json) => new GalleryData(
    totalImages: json["totalImages"] == null ? null : json["totalImages"],
    status: json["status"] == null ? null : json["status"],
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    image: json["image"] == null ? null : json["image"],
  );

  Map<String, dynamic> toJson() => {
    "totalImages": totalImages == null ? null : totalImages,
    "status": status == null ? null : status,
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "image": image == null ? null : image,
  };
}
