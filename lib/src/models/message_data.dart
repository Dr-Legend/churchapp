// To parse this JSON data, do
//
//     final messageData = messageDataFromJson(jsonString);

import 'dart:convert';

List<MessageData> messageDataFromJson(String str) => new List<MessageData>.from(
    json.decode(str).map((x) => MessageData.fromJson(x)));

String messageDataToJson(List<MessageData> data) =>
    json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class MessageData {
  int totalRecord;
  String status;
  int id;
  String name;
  String image;
  String messageTime;
  String messageDate;
  String content;

  MessageData({
    this.totalRecord,
    this.status,
    this.id,
    this.name,
    this.image,
    this.messageTime,
    this.messageDate,
    this.content,
  });

  factory MessageData.fromJson(Map<String, dynamic> json) => new MessageData(
        totalRecord: json["totalRecord"] == null ? null : json["totalRecord"],
        status: json["status"] == null ? null : json["status"],
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        image: json["image"] == null ? null : json["image"],
        messageTime: json["messageTime"] == null ? null : json["messageTime"],
        messageDate: json["messageDate"] == null ? null : json["messageDate"],
        content: json["content"] == null ? null : json["content"],
      );

  Map<String, dynamic> toJson() => {
        "totalRecord": totalRecord == null ? null : totalRecord,
        "status": status == null ? null : status,
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "image": image == null ? null : image,
        "messageTime": messageTime == null ? null : messageTime,
        "messageDate": messageDate == null ? null : messageDate,
        "content": content == null ? null : content,
      };
}
