// To parse this JSON data, do
//
//     final upcomingEventData = upcomingEventDataFromJson(jsonString);

import 'dart:convert';

List<UpcomingEventData> upcomingEventDataFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<UpcomingEventData>.from(
      jsonData.map((x) => UpcomingEventData.fromJson(x)));
}

String upcomingEventDataToJson(List<UpcomingEventData> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class UpcomingEventData {
  String status;
  String id;
  String name;
  String image;
  String eventTime;
  String eventDate;
  String content;

  UpcomingEventData({
    this.status,
    this.id,
    this.name,
    this.image,
    this.eventTime,
    this.eventDate,
    this.content,
  });

  factory UpcomingEventData.fromJson(Map<String, dynamic> json) =>
      new UpcomingEventData(
        status: json["status"] == null ? null : json["status"],
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        image: json["image"] == null ? null : json["image"],
        eventTime: json["eventTime"] == null ? null : json["eventTime"],
        eventDate: json["eventDate"] == null
            ? null
            : json["eventDate"],
        content: json["content"] == null ? null : json["content"],
      );

  Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "image": image == null ? null : image,
        "eventTime": eventTime == null ? null : eventTime,
        "eventDate": eventDate == null ? null : eventDate,
        "content": content == null ? null : content,
      };
}
