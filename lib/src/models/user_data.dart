
class UserData {
  final int userId;
  final int id;
  final String title;
  final String body;

  UserData({this.userId, this.id, this.title, this.body});

  factory UserData.decodeJson(Map<String, dynamic> json) {
    return UserData(
      userId: json['userId'],
      id: json['id'],
      title: json['title'],
      body: json['body'],
    );
  }
}