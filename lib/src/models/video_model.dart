// To parse this JSON data, do
//
//     final videoData = videoDataFromJson(jsonString);

import 'dart:convert';

List<VideoData> videoDataFromJson(String str) => new List<VideoData>.from(json.decode(str).map((x) => VideoData.fromJson(x)));

String videoDataToJson(List<VideoData> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class VideoData {
  int totalRecord;
  Status status;
  int id;
  String name;
  String youtubevideoid;
  String thumbnail;
  String videoDate;
  CategoryName categoryName;

  VideoData({
    this.totalRecord,
    this.status,
    this.id,
    this.name,
    this.youtubevideoid,
    this.thumbnail,
    this.videoDate,
    this.categoryName,
  });

  factory VideoData.fromJson(Map<String, dynamic> json) => new VideoData(
    totalRecord: json["totalRecord"] == null ? null : json["totalRecord"],
    status: json["status"] == null ? null : statusValues.map[json["status"]],
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    youtubevideoid: json["youtubevideoid"] == null ? null : json["youtubevideoid"],
    thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
    videoDate: json["videoDate"] == null ? null : json["videoDate"],
    categoryName: json["categoryName"] == null ? null : categoryNameValues.map[json["categoryName"]],
  );

  Map<String, dynamic> toJson() => {
    "totalRecord": totalRecord == null ? null : totalRecord,
    "status": status == null ? null : statusValues.reverse[status],
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "youtubevideoid": youtubevideoid == null ? null : youtubevideoid,
    "thumbnail": thumbnail == null ? null : thumbnail,
    "videoDate": videoDate == null ? null : videoDate,
    "categoryName": categoryName == null ? null : categoryNameValues.reverse[categoryName],
  };
}

enum CategoryName { ROCK_OF_LIFE }

final categoryNameValues = new EnumValues({
  "Rock of Life": CategoryName.ROCK_OF_LIFE
});

enum Status { SUCCESS }

final statusValues = new EnumValues({
  "success": Status.SUCCESS
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
