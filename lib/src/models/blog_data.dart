// To parse this JSON data, do
//
//     final blogData = blogDataFromJson(jsonString);

import 'dart:convert';

List<BlogData> blogDataFromJson(String str) {
  final jsonData = json.decode(str);
  return new List<BlogData>.from(jsonData.map((x) => BlogData.fromJson(x)));
}

String blogDataToJson(List<BlogData> data) {
  final dyn = new List<dynamic>.from(data.map((x) => x.toJson()));
  return json.encode(dyn);
}

class BlogData {
  String status;
  int id;
  String name;
  String image;
  String url;
  String blogDate;
  String content;

  BlogData({
    this.status,
    this.id,
    this.name,
    this.image,
    this.url,
    this.blogDate,
    this.content,
  });

  factory BlogData.fromJson(Map<String, dynamic> json) => new BlogData(
        status: json["status"],
        id: json["id"],
        name: json["name"],
        image: json["image"],
        url: json["url"],
        blogDate: json["blogDate"],
        content: json["content"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "id": id,
        "name": name,
        "image": image,
        "url": url,
        "blogDate": blogDate,
        "content": content,
      };
}
