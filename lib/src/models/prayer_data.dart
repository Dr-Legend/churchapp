// To parse this JSON data, do
//
//     final prayerData = prayerDataFromJson(jsonString);

import 'dart:convert';

PrayerData prayerDataFromJson(String str) {
  final jsonData = json.decode(str);
  return PrayerData.fromJson(jsonData);
}

String prayerDataToJson(PrayerData data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class PrayerData {
  String status;

  PrayerData({
    this.status,
  });

  factory PrayerData.fromJson(Map<String, dynamic> json) => new PrayerData(
    status: json["status"] == null ? null : json["status"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
  };
}
