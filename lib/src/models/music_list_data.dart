// To parse this JSON data, do
//
//     final musicListData = musicListDataFromJson(jsonString);

import 'dart:convert';

List<MusicListData> musicListDataFromJson(String str) => new List<MusicListData>.from(json.decode(str).map((x) => MusicListData.fromJson(x)));

String musicListDataToJson(List<MusicListData> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class MusicListData {
  String status;
  int id;
  String name;
  String poster;
  String track;

  MusicListData({
    this.status,
    this.id,
    this.name,
    this.poster,
    this.track,
  });

  factory MusicListData.fromJson(Map<String, dynamic> json) => new MusicListData(
    status: json["status"] == null ? null : json["status"],
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    poster: json["poster"] == null ? null : json["poster"],
    track: json["track"] == null ? null : json["track"],
  );

  Map<String, dynamic> toJson() => {
    "status": status == null ? null : status,
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "poster": poster == null ? null : poster,
    "track": track == null ? null : track,
  };
}
