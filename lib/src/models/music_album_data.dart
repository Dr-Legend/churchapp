// To parse this JSON data, do
//
//     final musicAlbumData = musicAlbumDataFromJson(jsonString);

import 'dart:convert';

List<MusicAlbumData> musicAlbumDataFromJson(String str) =>
    new List<MusicAlbumData>.from(
        json.decode(str).map((x) => MusicAlbumData.fromJson(x)));

String musicAlbumDataToJson(List<MusicAlbumData> data) =>
    json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class MusicAlbumData {
  int totalRecord;
  String status;
  int id;
  String name;
  String image;
  String albumDate;

  MusicAlbumData({
    this.totalRecord,
    this.status,
    this.id,
    this.name,
    this.image,
    this.albumDate,
  });

  factory MusicAlbumData.fromJson(Map<String, dynamic> json) =>
      new MusicAlbumData(
        totalRecord: json["totalRecord"] == null ? null : json["totalRecord"],
        status: json["status"] == null ? null : json["status"],
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        image: json["image"] == null ? null : json["image"],
        albumDate: json["albumDate"] == null ? null : json["albumDate"],
      );

  Map<String, dynamic> toJson() => {
        "totalRecord": totalRecord == null ? null : totalRecord,
        "status": status == null ? null : status,
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "image": image == null ? null : image,
        "albumDate": albumDate == null ? null : albumDate,
      };
}
