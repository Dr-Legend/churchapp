import 'package:church_app/main.dart';
import 'package:church_app/src/screens/home/home_bloc.dart';
import 'package:church_app/src/screens/home/home_state.dart';
import 'package:church_app/src/screens/home/home_screen.dart';
import 'package:church_app/src/screens/settings/settings_bloc.dart';
import 'package:church_app/src/screens/settings/settings_screen.dart';
import 'package:church_app/src/screens/settings/settings_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Routes {
  static const String root = "/";
  static const String searchScreen = "/searchScreen";
  static const String settingsScreen = "/settingsScreen";
}

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Routes.root:
        // return MaterialPageRoute(builder: (_) => Login());
        return MaterialPageRoute(
            builder: (_) => BlocBuilder<HomeBloc, HomeState>(
                  builder: (BuildContext context, HomeState state) =>
                      MyHomePage(),
                ));

      case Routes.settingsScreen:
        return MaterialPageRoute(
          builder: (_) => BlocBuilder<SettingsBloc, SettingsState>(
            builder: (BuildContext context, SettingsState state) =>
                SettingsScreen(settingsState: state),
          ),
        );
      case Routes.searchScreen:
        return MaterialPageRoute(builder: (_) => ChurchApp());
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                      child: Text('No route defined for ${settings.name}')),
                ));
    }
  }
}
