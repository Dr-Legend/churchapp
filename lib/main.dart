import 'dart:ui';

import 'package:banner_view/banner_view.dart';
import 'package:bloc/bloc.dart';
import 'package:church_app/src/screens/blog/blog_bloc.dart';
import 'package:church_app/src/screens/home/home_bloc.dart';
import 'package:church_app/src/screens/pdf_screen/pdf_screen_bloc.dart';
import 'package:church_app/src/screens/settings/settings_bloc.dart';
import 'package:church_app/src/church_app_colors.dart';
import 'package:church_app/src/router.dart';
import 'package:church_app/src/screens/testimony/testimony_bloc.dart';
import 'package:church_app/src/widgets/main_drawer.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'package:gradient_bottom_navigation_bar/gradient_bottom_navigation_bar.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:church_app/src/widgets/material_buttons.dart' as MaterialButton;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  BlocSupervisor.delegate = await HydratedBlocDelegate.build();

  final libraryDir = await getApplicationDocumentsDirectory();
//  Hive.init("${libraryDir.path}/hive");

//  PubHtmlParsingClient _htmlParsingClient;
  final getIt = GetIt.instance;
//  _htmlParsingClient = PubHtmlParsingClient();
//  getIt
//    ..registerSingleton<PubHtmlParsingClient>(_htmlParsingClient)
//    ..registerSingleton<FullPackageRepository>(
//      FullPackageRepository(client: _htmlParsingClient),
//    );
  runApp(MultiProvider(
    providers: [
      BlocProvider<SettingsBloc>(
        builder: (BuildContext context) => SettingsBloc(),
      ),
      BlocProvider<HomeBloc>(
        builder: (BuildContext context) => HomeBloc(),
      ),
      BlocProvider<BlogBloc>(
        builder: (BuildContext context) => BlogBloc(),
      ),
      BlocProvider<TestimonyBloc>(
        builder: (BuildContext context) => TestimonyBloc(),
      ),
      BlocProvider<PdfScreenBloc>(
        builder: (BuildContext context) => PdfScreenBloc(),
      ),
    ],
    child: ChurchApp(),
  ));
}

class ChurchApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return DynamicTheme(
      defaultBrightness: Brightness.light,
      data: (brightness) => ThemeData(
        indicatorColor: ChurchAppColors.lightBlue,
        accentColor: ChurchAppColors.lightBlue,
        brightness: brightness,
        primarySwatch: ChurchAppColors.lightBlue,
        primaryColorBrightness: brightness,
        backgroundColor: ChurchAppColors.scaffoldBackground,
        appBarTheme: AppBarTheme(
          brightness: brightness,
          color: brightness == Brightness.dark
              ? ChurchAppColors.darkAccent
              : Colors.white,
        ),
      ),
      themedWidgetBuilder: (context, theme) {
        return MaterialApp(
          title: 'Church App',
          debugShowCheckedModeBanner: false,
          theme: theme,
          onGenerateRoute: Router.generateRoute,
          initialRoute: Routes.root,
        );
      },
    );
  }
}
